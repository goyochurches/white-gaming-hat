import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, NavigationCancel, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'my-first-project';
  constructor(
    private router: Router

    ){

    }


    ngOnInit(){

    }

    gotoItems(category) {
      this.router.navigate([`/home/${category}`]);

    }
}
