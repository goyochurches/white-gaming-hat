import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of, Observable, Subject } from 'rxjs';
import { Type } from '../model/type.model';
import { tap } from "rxjs/operators";
@Injectable({
  providedIn: 'root'
})
export class GamesService {

  private _refresh$ = new Subject<void>();
  constructor(private http: HttpClient) {}

  get refresInfo() {
    return this._refresh$;
  }
  get(): Observable<Type[]> {
    const headers ={  'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8' }
    return this.http.get<Type[]>(`http://stage.whgstage.com/front-end-test/games.php`, {headers})    .pipe(
      tap(()=>{
        this._refresh$.next();
      })
    )
  }

}
