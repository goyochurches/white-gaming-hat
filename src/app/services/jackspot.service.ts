import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of, Observable, Subject } from 'rxjs';
import { Type } from '../model/type.model';
import { JackSpot } from '../model/jackspot.model';
import { tap } from "rxjs/operators";



@Injectable({
  providedIn: 'root'
})
export class JackSpotService {

  private _refresh$ = new Subject<void>();
  constructor(private http: HttpClient) { }


  get refresInfo() {
    return this._refresh$;
  }
  get(): Observable<JackSpot[]> {
    const headers ={  'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8' }
    return this.http.get<JackSpot[]>(`http://stage.whgstage.com/front-end-test/jackpots.php`, { headers })
    .pipe(
      tap(()=>{
        this._refresh$.next();
      })

    )
  }
}
