import { Component, ElementRef, OnInit, QueryList, Renderer2, ViewChildren, Inject, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JackSpot } from '../model/jackspot.model';
import { Type } from '../model/type.model';
import { GamesService } from '../services/games.service';
import { JackSpotService } from '../services/jackspot.service';
import { DOCUMENT } from '@angular/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  listTypes: Type[] = [];
  listJackSpot: JackSpot[] = [];
  listTypeFilter: Type[] = [];
  category: string = "";
  category2: string = "";
  category3: string = "";
  listClass: string[];
  listParams: string[] = [];
  arrayCategories: string[] = [];
  @ViewChildren("divMessages") divMessages: QueryList<ElementRef>;
  @ViewChildren("ribbon") divRibbon: QueryList<ElementRef>;
  suscription: Subscription;
  clearInterval;


  constructor(
    private _gameService: GamesService,
    private _jackSpotService: JackSpotService,
    private activatedRoute: ActivatedRoute,
    private renderer: Renderer2


  ) {
    this.category = this.activatedRoute.snapshot.params.category;
    this.category2 = this.activatedRoute.snapshot.params.category2;
    this.category3 = this.activatedRoute.snapshot.params.category3;

  }

  ngOnInit(): void {
    this.getGames();


   this.clearInterval =  setInterval(() => {
     this.updateJackSpots()
    }, 1000);
  }
  ngOnDestroy(): void {
    this.suscription.unsubscribe();
    clearInterval(this.clearInterval);
  }

  ngAfterViewInit() {
    this.addTitle();
  }
  updateJackSpots(){
    this.getJackSpots();
    this.orderGameByAmount();
    this.divMessages.forEach(element => {
      this.listJackSpot.forEach(item => {
        if (item.game === element.nativeElement.classList.value) {
          this.renderer.setProperty(element.nativeElement, 'innerHTML', "<div style='border-radius:1px;position: absolute;bottom: 10; background: black;width: 79%; transition: 0.5s ease;opacity: 0;color: white;font-size: 20px;font-weight: bolder;padding: 3px;text-align: center; opacity: 0.5;><p class='text-center '>£" + item.amount + "</p></div>");
        }
      })
    });
  }
  getGames() {
    this.getJackSpots();
    this._gameService.get().subscribe((games) => {
      this.listTypes = games;
      if (this.category2 != undefined) {
        this.arrayCategories.push(this.category);
        this.arrayCategories.push(this.category2);
        this.arrayCategories.push(this.category3);

        this.filterBy(this.arrayCategories);
      } else {
        if (this.category != undefined) {
          this.arrayCategories.push(this.category);
          this.filterBy(this.arrayCategories);

        }
      }
    })
  }
  getJackSpots() {
    this.suscription = this._jackSpotService.get().subscribe((jackspot) => {
      this.listJackSpot = jackspot;
    },
      error => {
        console.log(error);
      });
  }

  addTitle() {
    this.divMessages.changes.subscribe((r) => {
      r._results.forEach(element => {
        this.listJackSpot.forEach(item => {
          if (item.game === element.nativeElement.classList.value) {
            this.renderer.setProperty(element.nativeElement, 'innerHTML', "<div style='border-radius:1px;position: absolute;bottom: 10; background: black;width: 74%; transition: 0.5s ease;opacity: 0;color: white;font-size: 20px;font-weight: bolder;padding: 3px;text-align: center; opacity: 0.5;><p class='text-center '>£" + item.amount + "</p></div>");
          }
        })
      });
    });
  }

  filterBy(listCategory: string[]) {

    if (listCategory != undefined) {
      this.listTypes.filter((item: Type) => {
        listCategory.forEach(elementCategory => {
          item.categories.forEach(element => {
            if (element.trim() == elementCategory) {
              this.listTypeFilter.push(item);
            }
          });
        });
      });
    }

    this.listTypes = this.listTypeFilter;
    this.filterByRibbon();
    this.orderGameByAmount();
  }

  filterByRibbon() {
    this.listTypes.forEach(item => {
      if (item.categories.includes("new") && item.categories.includes("top")) {

        this.divRibbon.changes.subscribe(elementRibbont => {
          elementRibbont._results.forEach(element => {
            if("ribbon_"+item.id==element.nativeElement.classList.value)
            this.renderer.setProperty(element.nativeElement, 'innerHTML', "<img  style='width:60px;z-index:2;position:absolute; top:0;right:30px;' src='https://www.freeiconspng.com/uploads/corner-ribbon-png----------------------5.png' />");
          })
        })
      }
    })
  }

  orderGameByAmount() {
    const arrComun = []
    for (let x of this.listTypes) {
      const found = this.listJackSpot.filter(y => y.game === x.id).shift();
      if (found)
        arrComun.push({ ...x, ...found });
    }
    arrComun.push(... this.listTypes.filter(z => this.listJackSpot.map(x => x.game).indexOf(z.id) === -1))
    arrComun.sort(function (a, b) {
      if (typeof parseFloat(a.amount) === 'number') {
        return parseFloat(b.amount) - parseFloat(a.amount);
      } else {
        return 0;
      }
    });
    this.listTypes = arrComun;
  }
}
